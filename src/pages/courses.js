import SampleCourse from '../Components/SampleCourse'
import coursesData from '../data/coursesData'
import {Fragment} from 'react'

export default function Courses() {

	// console.log(coursesData)
	// console.log(coursesData[0])


	const courses = coursesData.map(course => {
		return (
		<SampleCourse key={course.id} courseProp={course}/>
		)
	})



	return (

		<Fragment>
				{courses}
		</Fragment>

		)
}