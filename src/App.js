import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import {Fragment} from 'react'
import AppNavbar from './Components/AppNavbar';
// import Banner from './Components/Banner'
// import Highlights from './Components/Highlights'
import Home from './pages/home'
import Logout from './pages/logout'
import Courses from './pages/courses'
import Register from './pages/register'
import Login from './pages/login'
import NotFound from './pages/notfound'
import './App.css';






function App() {
  return (
    <Router>
    <AppNavbar/>
    <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="*" element={<NotFound/>}/>
          </Routes>
          </Container>
    </Router>
  );
}

export default App;
